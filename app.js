const mongodb = require('mongodb');

const data = require('./data.json');
const games = data.games; // that's what we need actually

const url = 'mongodb://localhost:27017/learnMongo';
const dbName = 'learnMongo'; // let's say that's our database name
// not needed however

// use the new url parser to avoid warning messages
const client = new mongodb.MongoClient(url, { useNewUrlParser: true });

// I'm using asyn await and try catch
// because I hate promise and callback hell .__.
const app = async() => {
  try {
    await client.connect();
    console.log('Connected to the database server!');

    // define indexes
    // here 1 means to index in the ascending order
    // -1 would mean to index in the descending order
    const db = client.db();
    try {
      await db.collection('games').createIndex({
        name: 1,
        platform: 1
      }, {
        unique: true
      });
    } catch (e) {
      console.log('Defined Index already exists, skipping ....');
    }

    // time for crud

    // add a single element
    const singleDoc = {
      name: 'Red Dead Redemption 2',
      platform: 'PS4',
      publisher: 'Rockstar'
    };
    await addOneDocument(singleDoc, db);

    // add multiple
    await addMultipleDocs(games, db);

    // fetch all
    const docs = await readAllDocs(db);
    console.log(docs);

    // search
    const searchQuery = {
      publisher: 'Rockstar'
    };
    const gamesFound = await searchInDb(db, searchQuery);
    console.log(gamesFound);

    // update
    const docToUpdate = { name: 'Halo MCC' };
    const updateTerm = { $set: { name: 'Halo Master Chief Collection' } };

    console.log('Before update :');
    let haloMcc = await db.collection('games').findOne(docToUpdate);
    console.log(haloMcc);

    await updateDoc(db, docToUpdate, updateTerm);
    // check
    haloMcc = await db.collection('games').findOne({
      name: 'Halo Master Chief Collection'
    });
    console.log('After update :')
    console.log(haloMcc);

    // delete a doc
    const filter = {
      name: 'Red Dead Redemption 2',
      platform: 'XBOXONE'
    };

    await deleteDoc(db, filter);


    console.log('Closing connection ...');
    client.close();
  } catch (err) {
    // console.log('Error connecting to the database.');
    console.log(err.toString());
  }
};

// envoke app
app();

// db functions
const addOneDocument = async(doc, db) => {
  try {
    // const db = client.db();

    await db.collection('games').insertOne(doc);
    console.log('Document has been updated! ...');
  } catch (e) {
    console.log(e.toString());
  }
};

const addMultipleDocs = async(docs, db) => {
  try {
    // const db = client.db();

    await db.collection('games').insertMany(docs);
    console.log('Multiple Docs Added!');
  } catch (e) {
    console.log(e.toString());
  }
};

const readAllDocs = async(db) => {
  try {
    const docs = await db.collection('games').find({});
    return docs.toArray();
  } catch (e) {
    console.log(e.toString());
  }
};

const searchInDb = async(db, searchQuery) => {
  try {
    const docs = await db.collection('games').find(searchQuery);
    return docs.toArray();
  } catch (e) {
    console.log(e.toString());
  }
};

const updateDoc = async(db, docToUpdate, updateTerm) => {
  try {
    await db.collection('games').findOneAndUpdate(docToUpdate, updateTerm);
    return updatedDoc;
  } catch (e) {
    console.log(e.toString());
  }
};

const deleteDoc = async(db, filter) => {
  try {
    await db.collection('games').findOneAndDelete(filter);
    console.log('Document deleted...');
  } catch (e) {
    console.log(e.toString());
  }
};
